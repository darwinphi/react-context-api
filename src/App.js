import React, { Component } from 'react';

// 1. Make a new context
const MyContext = React.createContext()

// 2. Create a provider component
class MyProvider extends Component {
	state = {
		name: 'Darwin',
		age: 22,
		cool: true
	}

	render() {
		return (
			<MyContext.Provider value={{
				state: this.state,
				growAYearOlder: () => this.setState({
					age: this.state.age + 1
				})
			}}>
				{ this.props.children }
			</MyContext.Provider>
		)
	}
}

const Family = (props) => (
	<div>
		<Person />
	</div>
)

class Person extends Component {
	render() {
		return(
			<div>
				<MyContext.Consumer>
					{(context) => (
						// Instead of `div` we can use `<React.Fragment>`, It's like a blank tag
						<React.Fragment>
							<p>Name:{context.state.name}</p>
							<p>Age:{context.state.age}</p>
							<button onClick={context.growAYearOlder}>Grow Old</button>
							
						</React.Fragment>
					)}
				</MyContext.Consumer>
			</div>
		)
	}	
}

const Old = () => (
	<div>
		<p>You're old</p>
	</div>
)
class Girlfriend extends Component {
	render() {
		return (
			<MyContext.Consumer>
				{	
					(context) => (
						(context.cool) && <Old />
					)
				}
			</MyContext.Consumer>
		)
	}
}

class App extends Component {
  render() {
    return (
    	<MyProvider>
	      <div>
	        <p>I'm App</p>
	        <Family />
	        <Girlfriend />
	      </div>
      </MyProvider>
    );
  }
}

export default App;
